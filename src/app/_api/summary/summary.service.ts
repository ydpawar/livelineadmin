import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable()
export class SummaryService {
  constructor(private http: HttpService) { }

  plusUsers(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement/plus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement/plus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  plusUsersLive(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement-live/plus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement-live/plus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  minusUsers(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement/minus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement/minus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  minusUsersLive(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement-live/minus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement-live/minus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  doSettlement(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'settlement/clear-settlement';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  doSettlementAdmin(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'settlement/clear-settlement-admin';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  // system
  systemPlusUsers(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement/system-plus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement/system-plus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  systemMinusUsers(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (uid) {
      httpRequestModel.url = 'settlement/system-minus-account/' + uid;
    } else {
      httpRequestModel.url = 'settlement/system-minus-account';
    }
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  doSystemSettlement(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'settlement/system-clear-settlement';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

}
