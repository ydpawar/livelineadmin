import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LeftSidebarComponent } from './left-sidebar.component';


const routes: Routes = [
  {
    path: '',
    component: LeftSidebarComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ], exports: [
    RouterModule
  ],
})
export class LeftSidebarModule {
}
