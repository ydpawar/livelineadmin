import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {BaseComponent} from '../../../../common/commonComponent';
import {FormGroup, Validators} from '@angular/forms';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  name: string;
  sportId: string;
  eventId: string;
  pl: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  title = 'System Live Game Profit Loss';
  breadcrumb: any = [{title: 'System Live Game Profit Loss', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };

  uid = '';
  sid = '';
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_teenpatti_pl';

  constructor(
      inj: Injector,
      private service: HistoryService,
  ) {
    super(inj);
    this.sid = this.activatedRoute.snapshot.params.sid;
    this.uid = this.activatedRoute.snapshot.params.uid;
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
    // tslint:disable-next-line:triple-equals
    if ( this.sid == '999' ) {
      this.title = 'System Live Game 2 Profit Loss';
    }
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    const data = {sid: this.sid, uid: this.uid};
    this.service.systemTeenpattiProfitLoss(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_teenpatti_pl').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        if ( this.sid === '999' ) {
          this.title = 'System Live Game 2 Profit Loss From W/L';
        } else {
          this.title = 'System Live Game Profit Loss From W/L';
        }
      }

      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.sportId = item.sportId;
            cData.eventId = item.eventId;
            cData.pl = item.profitLoss;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        if (res.total) {
          this.aListTotal = res.total;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_teenpatti_pl');
      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      sid: [this.sid],
      uid: [this.uid],
    });
  }

  submitForm( fType = null ) {
    if ( fType != null ) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType;
      this.service.systemTeenpattiProfitLoss(data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.systemTeenpattiProfitLoss(data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      this.onSuccess(res);
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

}

