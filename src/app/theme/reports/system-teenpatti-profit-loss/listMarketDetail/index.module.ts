import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListMarketDetailComponent } from './index.component';

const routes: Routes = [
  {
    path: 'market-detail/:sid/:eid',
    component: ListMarketDetailComponent
  },
  {
    path: 'market-detail/:sid/:eid/:uid',
    component: ListMarketDetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    ListMarketDetailComponent
  ]
})
export class ListMarketDetailModule {

}
