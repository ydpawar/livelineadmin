import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {BaseComponent} from '../../../../common/commonComponent';
import {Validators} from '@angular/forms';
import {ExcelService} from '../../../../common/excel.service';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  uid: string;
  title: string;
  description: string;
  time: string;
  ip: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  uid: string;
  title: string;
  description: string;
  time: string;
  ip: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'Activity Log';
  breadcrumb: any = [{title: 'Activity Log', url: '/' }, {title: 'List', url: '/' }];
  page = { start: 1, end: 5 };
  uid = '';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_chip';

  constructor(
      inj: Injector,
      private service: HistoryService,
      private excelSheet: ExcelService
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.activityLog().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_chip').dataTable().fnDestroy();
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();
            cData.uid = item.uid;
            cData.title = item.title;
            cData.description = item.description;
            cData.time = item.created_on;
            cData.ip = item.ip_address;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_chip');

      }
      this.spinner.hide();
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

}

