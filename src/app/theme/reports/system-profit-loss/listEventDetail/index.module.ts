import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListEventDetailComponent } from './index.component';

const routes: Routes = [
  {
    path: 'event-detail/:sid',
    component: ListEventDetailComponent
  },
  {
    path: 'event-detail/:sid/:uid',
    component: ListEventDetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    ListEventDetailComponent
  ]
})
export class ListEventDetailModule {

}
