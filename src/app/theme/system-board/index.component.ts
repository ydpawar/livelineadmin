import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../../_api';
import {AuthIdentityService, ToastrService} from '../../_services';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [DashboardService]
})
export class SystemBoardComponent implements OnInit {

  dataList: any = [];
  eventList: any = [];
  marketList: any = [];
  marketIdsArr: any = [];
  isActiveTabs: string;
  isEmpty = true;
  systemId: string;
  systemUid: string;
  systemName: string = 'White Label';
  cUserData: any;

  title = 'System Dashboard';
  breadcrumb: any = [];
  // breadcrumb: any = [{title: 'Manage', url: '/' }];

  // tslint:disable-next-line:max-line-length
  constructor(
      private service: DashboardService,
      private route: ActivatedRoute,
      private authIdentity: AuthIdentityService,
      private spinner: NgxSpinnerService,
      private toaster: ToastrService) {
      this.isActiveTabs = 'all';

      this.systemId = this.route.snapshot.params.oid;

  }

  ngOnInit() {
    this.getDataList(this.isActiveTabs);
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.cUserData = auth.getIdentity();
    }
  }

  async getDataList(type) {
    this.spinner.show();
    const data = {systemId: this.systemId };
    await this.service.getListBySystem(type, data).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccessDataList(data);
        },
        error => {
          // this.toaster.error('Error in Get DataList Api !!', 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.isEmpty = true;
        this.eventList = this.marketList = this.dataList = [];

        if ( response.data.length > 0 ) {
          this.dataList = response.data;
        }

        if ( response.event && response.event.length > 0 ) {
            this.isEmpty = false;
            this.eventList = response.event;
        }

        if ( response.sport.length > 0 ) {
          this.isEmpty = false;
          this.marketList = response.sport;
        }

        if ( response.system ) {
          this.systemName = response.system.systemName;
          this.systemUid = response.system.uid;
        }

      }
    }
    this.spinner.hide();
  }

}
