import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SystemBoardComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: SystemBoardComponent,
  },
  {
    path: ':oid',
    component: SystemBoardComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SystemBoardComponent
  ]
})
export class SystemBoardModule {
}
