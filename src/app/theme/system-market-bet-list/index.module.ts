import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { SystemMarketBetListComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: SystemMarketBetListComponent,
  },
  {
    path: ':mtype/:eid',
    component: SystemMarketBetListComponent,
  },
  {
    path: ':mtype/:eid/:mid',
    component: SystemMarketBetListComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, InfiniteScrollModule
  ], exports: [
    RouterModule
  ], declarations: [
    SystemMarketBetListComponent
  ]
})
export class SystemMarketBetListModule {
}
