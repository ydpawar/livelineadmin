import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ClientService} from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ClientService]
})
export class SettingComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Client - Setting';
  breadcrumb: any = [{title: 'Client', url: '/' }, {title: 'Setting', url: '/' }];

  uid: string;

  frm: FormGroup;

  // tslint:disable-next-line:variable-name
  Cricket_Frm1: FormGroup;
  // tslint:disable-next-line:variable-name
  Cricket_Frm2: FormGroup;
  // tslint:disable-next-line:variable-name
  Cricket_Frm3: FormGroup;
  // tslint:disable-next-line:variable-name
  Football_Frm1: FormGroup;
  // tslint:disable-next-line:variable-name
  Football_Frm2: FormGroup;
  // tslint:disable-next-line:variable-name
  Football_Frm3: FormGroup;
  // tslint:disable-next-line:variable-name
  Tennis_Frm1: FormGroup;
  // tslint:disable-next-line:variable-name
  Tennis_Frm2: FormGroup;
  // tslint:disable-next-line:variable-name
  Tennis_Frm3: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private service: ClientService,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
    this.uid = this.route.snapshot.params.uid;
  }

  ngOnInit() {
    this.getData();
    this.createForm();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  getData() {
    this.service.limitSetting(this.uid).subscribe((res) => {
      this.intGetData(res);
    });
  }

  intGetData(res) {
    if (res.status === 1) {
      // tslint:disable-next-line:triple-equals
      if ( res.data != undefined ) {

        const cricketFrm1 = res.data.cricket_matchodd;
        this.Cricket_Frm1.patchValue({
          uid: res.data.uid,
          frm_type: 'cricketFrm1',
          min_stack: cricketFrm1.min_stack,
          max_stack: cricketFrm1.max_stack,
          max_profit_limit: cricketFrm1.max_profit_limit,
          max_expose_limit: cricketFrm1.max_expose_limit,
          bet_delay: cricketFrm1.bet_delay,
        });

        const cricketFrm2 = res.data.cricket_fancy;
        this.Cricket_Frm2.patchValue({
          uid: res.data.uid,
          frm_type: 'cricketFrm2',
          min_stack: cricketFrm2.min_stack,
          max_stack: cricketFrm2.max_stack,
          max_profit_limit: cricketFrm2.max_profit_limit,
          max_expose_limit: cricketFrm2.max_expose_limit,
          bet_delay: cricketFrm2.bet_delay,
        });

        const cricketFrm3 = res.data.cricket_bookmaker;
        this.Cricket_Frm3.patchValue({
          uid: res.data.uid,
          frm_type: 'cricketFrm3',
          min_stack: cricketFrm3.min_stack,
          max_stack: cricketFrm3.max_stack,
          max_profit_limit: cricketFrm3.max_profit_limit,
          max_expose_limit: cricketFrm3.max_expose_limit,
          bet_delay: cricketFrm3.bet_delay,
        });

        const footballFrm1 = res.data.football_matchodd;
        this.Football_Frm1.patchValue({
          uid: res.data.uid,
          frm_type: 'footballFrm1',
          min_stack: footballFrm1.min_stack,
          max_stack: footballFrm1.max_stack,
          max_profit_limit: footballFrm1.max_profit_limit,
          max_expose_limit: footballFrm1.max_expose_limit,
          bet_delay: footballFrm1.bet_delay,
        });

        const footballFrm2 = res.data.football_fancy;
        this.Football_Frm2.patchValue({
          uid: res.data.uid,
          frm_type: 'footballFrm2',
          min_stack: footballFrm2.min_stack,
          max_stack: footballFrm2.max_stack,
          max_profit_limit: footballFrm2.max_profit_limit,
          max_expose_limit: footballFrm2.max_expose_limit,
          bet_delay: footballFrm2.bet_delay,
        });

        const footballFrm3 = res.data.football_bookmaker;
        this.Football_Frm3.patchValue({
          uid: res.data.uid,
          frm_type: 'footballFrm3',
          min_stack: footballFrm3.min_stack,
          max_stack: footballFrm3.max_stack,
          max_profit_limit: footballFrm3.max_profit_limit,
          max_expose_limit: footballFrm3.max_expose_limit,
          bet_delay: footballFrm3.bet_delay,
        });

        const tennisFrm1 = res.data.tennis_matchodd;
        this.Tennis_Frm1.patchValue({
          uid: res.data.uid,
          frm_type: 'tennisFrm1',
          min_stack: tennisFrm1.min_stack,
          max_stack: tennisFrm1.max_stack,
          max_profit_limit: tennisFrm1.max_profit_limit,
          max_expose_limit: tennisFrm1.max_expose_limit,
          bet_delay: tennisFrm1.bet_delay,
        });

        const tennisFrm2 = res.data.tennis_fancy;
        this.Tennis_Frm2.patchValue({
          uid: res.data.uid,
          frm_type: 'tennisFrm2',
          min_stack: tennisFrm2.min_stack,
          max_stack: tennisFrm2.max_stack,
          max_profit_limit: tennisFrm2.max_profit_limit,
          max_expose_limit: tennisFrm2.max_expose_limit,
          bet_delay: tennisFrm2.bet_delay,
        });

        const tennisFrm3 = res.data.tennis_bookmaker;
        this.Tennis_Frm3.patchValue({
          uid: res.data.uid,
          frm_type: 'tennisFrm3',
          min_stack: tennisFrm3.min_stack,
          max_stack: tennisFrm3.max_stack,
          max_profit_limit: tennisFrm3.max_profit_limit,
          max_expose_limit: tennisFrm3.max_expose_limit,
          bet_delay: tennisFrm3.bet_delay,
        });

      }

      if ( res.userName !== undefined ) {
        this.title = 'Setting for ' + res.userName;
      }

    }
  }

  createForm() {
    this.Cricket_Frm1 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Cricket_Frm2 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Cricket_Frm3 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Football_Frm1 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Football_Frm2 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Football_Frm3 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Tennis_Frm1 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Tennis_Frm2 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

    this.Tennis_Frm3 = this.formBuilder.group({
      uid: [this.uid], frm_type: [''],
      min_stack: [ '', Validators.required],
      max_stack: [ '', Validators.required],
      max_profit_limit: [ '', Validators.required],
      max_expose_limit: [ '', Validators.required],
      bet_delay: [ '', Validators.required]
    });

  }

  submitCricketFrm1() {
    const data = this.Cricket_Frm1.value;
    if (this.Cricket_Frm1.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitCricketFrm2() {
    const data = this.Cricket_Frm2.value;
    if (this.Cricket_Frm2.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitCricketFrm3() {
    const data = this.Cricket_Frm3.value;
    if (this.Cricket_Frm3.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitFootballFrm1() {
    const data = this.Football_Frm1.value;
    if (this.Football_Frm1.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitFootballFrm2() {
    const data = this.Football_Frm2.value;
    if (this.Football_Frm2.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitFootballFrm3() {
    const data = this.Football_Frm3.value;
    if (this.Football_Frm3.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitTennisFrm1() {
    const data = this.Tennis_Frm1.value;
    if (this.Tennis_Frm1.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitTennisFrm2() {
    const data = this.Tennis_Frm2.value;
    if (this.Tennis_Frm2.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  submitTennisFrm3() {
    const data = this.Tennis_Frm3.value;
    if (this.Tennis_Frm3.valid) {
      this.service.limitSettingUpdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.getData();
    }
  }

  onCancel() {
    this._location.back();
  }

}

