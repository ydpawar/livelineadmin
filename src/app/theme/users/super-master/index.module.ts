import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SuperMasterComponent } from './index.component';

import {ManageComponent} from './manage/index.component';
import {CreateComponent} from './create/index.component';
import {ReferenceComponent} from './reference/index.component';

import {ManageModule} from './manage/index.module';
import {CreateModule} from './create/index.module';
import {ReferenceModule} from './reference/index.module';



const routes: Routes = [
  {
    path: '',
    component: SuperMasterComponent,
    children: [
      {
        path: '',
        component: ManageComponent
      },
      {
        path: 'manage',
        component: ManageComponent
      },
      {
        path: 'reference/:uid',
        component: ReferenceComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    ManageModule, CreateModule, ReferenceModule
  ], exports: [
    RouterModule
  ], declarations: [ SuperMasterComponent ]
})
export class SuperMasterModule {

}
