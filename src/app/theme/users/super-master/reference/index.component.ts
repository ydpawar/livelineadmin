import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ActionService, SuperMasterService} from '../../../../_api/index';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services/index';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
interface User {
  id: string;
  name: string;
  username: string;
  pl: string;
  plBalance: string;
  balance: string;
  pName: string;
  remark: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
  isLock: {
    text: string;
    class1: string;
    class2: string;
  };
  smCount: string;
  mCount: string;
  cCount: string;
}

class UserObj implements User {
  id: string;
  name: string;
  username: string;
  pl: string;
  plBalance: string;
  balance: string;
  pName: string;
  remark: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
  isLock: {
    text: string;
    class1: string;
    class2: string;
  };
  smCount: string;
  mCount: string;
  cCount: string;
}


@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SuperMasterService, ActionService]
})

export class ReferenceComponent implements OnInit, AfterViewInit, OnDestroy {

  uid: string;
  page = { start: 1, end: 5 };
  user: any;
  aList: User[] = [];
  cItem: User;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_sm';

  title = 'Reference Super Master';
  createUrl = '/users/super-master/create';
  breadcrumb: any = [{title: 'Super Master', url: '/' }, {title: 'Reference', url: '/' }];

  constructor(
      private service: SuperMasterService,
      private service2: ActionService,
      private loadJs: ScriptLoaderService,
      private route: ActivatedRoute,
      private spinner: NgxSpinnerService,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
      // tslint:disable-next-line:triple-equals
      if ( this.user.role == 'M1' || this.user.role == 'M2' || this.user.role == 'SM2') {
        this._location.back();
      }
    }
    this.uid = this.route.snapshot.params.uid;
    // tslint:disable-next-line:triple-equals
    if ( window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined ) {
      this.title = 'Reference Super Master - ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.reference(this.uid).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_sm').dataTable().fnDestroy();
        const items = res.data;
        const data: User[] = [];

        if (items.length > 0) {

          this.aList = [];
          for (const item of items) {
            const cData: User = new UserObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.username = item.username;
            cData.pl = item.pl;
            cData.plBalance = item.pl_balance;
            cData.balance = item.balance;
            cData.remark = item.remark;
            cData.pName = item.pName;
            cData.smCount = item.smCount;
            cData.mCount = item.mCount;
            cData.cCount = item.cCount;
            cData.isBlock = {
              text: item.isBlock === 1 ? 'Unblock' : 'Block',
              class1: item.isBlock === 1 ? 'success' : 'danger',
              class2: item.isBlock === 1 ? 'mdi-check' : 'mdi-close'
            };
            cData.isLock = {
              text: item.isLock === 1 ? 'Bet Unlock' : 'Bet Lock',
              class1: item.isLock === 1 ? 'success' : 'danger',
              class2: item.isLock === 1 ? 'mdi-toggle-switch' : 'mdi-toggle-switch-off'
            };

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
    this.spinner.hide();
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_sm').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  doStatusUpdate(uid, typ) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if ( typ === 1 ) {
          this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        } else {
          this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        }
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}

