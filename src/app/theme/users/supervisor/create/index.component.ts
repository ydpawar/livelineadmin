import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SupervisorService } from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SupervisorService]
})
export class CreateComponent implements OnInit, AfterViewInit {

  title = 'Supervisor - Create';
  breadcrumb: any = [{title: 'Supervisor', url: '/' }, {title: 'Create', url: '/' }];

  frm: FormGroup;
  userError: string;
  parentUser: string;

  constructor(
      private formBuilder: FormBuilder,
      private service: SupervisorService,
      private router: Router,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngAfterViewInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      username: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      remark: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
      // this.router.navigate(['/manage']);
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmName() { return this.frm.get('name'); }
  get frmUsername() { return this.frm.get('username'); }
  get frmPassword() { return this.frm.get('password'); }
  get frmRemark() { return this.frm.get('remark'); }

}

